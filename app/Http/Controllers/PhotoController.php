<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PhotoController extends Controller
{
    function saveData(Request $req)
    {
        $req->validate([
            'titolo' => 'required',
            'image' => 'required'
        ]);
        $ext = $req->image->guessExtension();
        if ($ext == 'jpg' || $ext == "png") {
            $name =  $req->titolo;
            $path = $name . "." . $ext;
            $destination = 'public';
            $scan = '../storage/app/public';
            $images = scandir($scan);
            foreach ($images as $image) {
                if (substr($image, 0, -4) == $name) {
                    return "Titolo già usato";
                }
            }
            if (count($images) >= 11) {
                return "Hai già inserito 9 immagini";
            } else {
                if ($req->image->storeAs($destination, $path)) {
                    return redirect('/');
                } else {
                    return "Immagine non caricata";
                }
            }
        } else {
            return "Formato non valido";
        }
    }
}