<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GalleryController extends Controller
{
    function getData()
    {
        $dir = "storage/";
        $images = scandir($dir);
        unset($images[0]);
        unset($images[1]);
        return view("gallery", compact('images'));
    }
}