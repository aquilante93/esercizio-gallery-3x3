<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Document</title>

</head>

<body>

    <div class="navbar">
        <ul>
            <li><a href="carica_foto">Carica foto</a></li>
            <li><a href="#">Galleria</a></li>
        </ul>
    </div>

    <div class="galleria">
        <h1>La tua galleria</h1>
        @foreach ($images as $image)
        <div><img src="<?php echo asset("storage/$image") ?>" alt=""></div>
        @endforeach

    </div>
</body>

</html>